#!/usr/bin/env sh
registry_port=${REGISTRY_PORT:=41906}
git_sha="$(git rev-parse HEAD)"
image_tag="k3d-registry.localhost:$registry_port/apbatch_web:${git_sha}"
target=${APBATCH_DOCKERFILE_TARGET:=development}

echo "Building apbatch image..."
docker build --target "$target" -t "$image_tag" -f Dockerfile .

echo "Pushing apbatch image to local registry..."
docker push "$image_tag"
