#!/usr/bin/env sh
pod="$(kubectl get pods --namespace=apbatch-development "--selector=app.kubernetes.io/name=apbatch" --no-headers | cut -d " " -f 1)"

kubectl exec -it --namespace=apbatch-development "$pod" -- /bin/sh

