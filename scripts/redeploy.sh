#!/usr/bin/env sh
registry_port=${REGISTRY_PORT:=41906}
image_tag="k3d-registry.localhost:$registry_port/apbatch_war"
remote_war="/apbatch/dist/apbatch.war"
local_war="tmp/apbatch.war"
user_creds="tomcat:tomcat"
tomcat_host="apbatch.k3d.localhost"
manager_path="manager/text/deploy?path=/apbatch&update=true"
tomcat_url="http://$tomcat_host/$manager_path"

echo "Building apbatch image..."
docker build --target "builder" -t "$image_tag" -f Dockerfile .

if test ! -d tmp/; then
  echo "Creating tmp directory for war file"
  mkdir tmp
fi

echo "Copying apbatch war to localhost..."
docker run -it --rm \
  -v "$(pwd)"/tmp:/tmp \
  --entrypoint "cp" \
  "$image_tag" \
  "$remote_war" \
  "/tmp/apbatch.war"

echo "Deploying apbatch war to tomcat..."
curl \
  --header "Host: $tomcat_host" \
  --user "$user_creds" \
  --upload-file "$local_war" \
  "${tomcat_url}"

