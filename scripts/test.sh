#!/usr/bin/env sh
pod="$(kubectl get pods --namespace=apbatch-development "--selector=app.kubernetes.io/name=apbatch" --no-headers | cut -d " " -f 1)"

# TODO: if we ever have AP Batch Junit tests, follow a patter similar to this for rspec
# kubectl exec -it --namespace=apbatch-development "$pod" -- /bin/sh -c "bundle exec rspec $*"

