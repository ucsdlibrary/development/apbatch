FROM openjdk:8 as builder

RUN apt-get update && apt install -y ant

WORKDIR /apbatch
COPY . /apbatch
RUN ant clean webapp

FROM tomcat:8.5.68-jdk8 as production
MAINTAINER "Matt Critchlow <mcritchlow@ucsd.edu">

RUN apt-get update && apt install -y netcat locales

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

COPY --from=builder /apbatch/container/apbatch.xml /usr/local/tomcat/conf/Catalina/localhost/

RUN mkdir /apbatch
COPY --from=builder /apbatch/container/apbatch.properties /apbatch/
COPY --from=builder /apbatch/container/getApbatchToken.sh /apbatch/
COPY --from=builder /apbatch/container/getApbatchTokenQA.sh /apbatch/
COPY --from=builder /apbatch/container/entrypoint.sh /apbatch/

COPY --from=builder /apbatch/dist/apbatch.war /usr/local/tomcat/webapps/

ENTRYPOINT ["/apbatch/entrypoint.sh"]
CMD ["catalina.sh", "run"]

FROM production as development
COPY --from=builder /apbatch/container/development/tomcat-users.xml /usr/local/tomcat/conf
RUN cp -r /usr/local/tomcat/webapps.dist/manager /usr/local/tomcat/webapps/
COPY --from=builder /apbatch/container/development/manager-context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
