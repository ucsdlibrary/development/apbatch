# ApBatch

ApBatch is a tool to manage electrical invoices, vendors and indexes for UCSD Library.  It is converted from FoxPro to a web based application.

## Local Development
The most effective way to do local development is by using the [k3d][k3d] tool
maintained by Rancher to provision and run an instance of the [AP Batch Helm
chart][apbatch-helm] within a local [k3s][k3s] cluster on your workstation.

There is a `Makefile` in the repository which you can use to setup the cluster,
build and push images to the local image registry, and deploy.

You can run `make` to get a list of options, or review the `Makefile` directly.

Note: You will need to create a file in the `scripts` directory called `k3d-private.yaml` and populate it with the following yaml values (update with real secrets):

The values are set in LastPass in the `AP Batch Secrets` Secure Note.

You will want to modify things like `email.to` for your own local testing.

```yaml
oracleApi:
  qaSecret: qa-secret
  prodSecret: prod-secret

email:
  to: your-email-address-for-testing
  from: lib-apbatch-account
  username: username
  password: password

ldap:
  username: ldap-user
  password: ldap-pass

minio:
  accessKey: access-key
  bucket: apbatch
  bucketKey: apbatch.dmp
  endpoint: endpointUrl
  secretKey: secret-key
```

On Mac and Windows boxes, you will need to add the following to either /etc/hosts ( on a Mac ) or c:\windows\system32\drivers\etc\hosts ( on Windows )

127.0.0.1 k3d-registry.localhost\
127.0.0.1 apbatch.k3d.localhost

Run the application:

```sh
make deploy
```

Once deployed, you can point your browser to:

```
http://apbatch.k3d.localhost/apbatch/
```

### Local Development Workflow
Initially, you will want to use `make deploy` to get the Helm deployment in
place and ensure everything is setup properly in the k3s cluster via `k3d`.

However, when you are working on the application code itself, a much faster
solution to see your code changes reflected in the k3s cluster will be to use:

```
make redeploy
```

This task leverages the Tomcat manager application that is enabled in the
`development` target in the `Dockerfile`. It does the following:

* Builds a new `war` file for the application using the `builder` target
* Copies that `war` file to the `./tmp` folder on your host machine
* Uses `curl` to upload the `war` file to the running Tomcat manager application inside the cluster, and redeploys automatically

This is significantly faster than building a new container image and deploying
it into the cluster.

[cron]:https://en.wikipedia.org/wiki/Cron
[gitlab-schedule]:https://docs.gitlab.com/ee/ci/pipelines/schedules.html
[k3d]:https://github.com/rancher/k3d/
[k3s]:https://github.com/k3s-io/k3s
[apbatch-helm]:./chart/README.md
