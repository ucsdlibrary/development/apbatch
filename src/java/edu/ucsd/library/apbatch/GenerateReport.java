package edu.ucsd.library.apbatch;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.json.simple.JSONObject;
import java.io.PrintWriter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class GenerateReport extends HttpServlet {
	private static Logger log = Logger.getLogger( SendOutputFiles.class );

	public void doGet(HttpServletRequest request, 
			HttpServletResponse response) {
		doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response){
		HttpSession session = request.getSession();
		String transmitData = (session.getAttribute("transmitData") != null ) ? (String)session.getAttribute("transmitData") : null;
		boolean success = false;
		if(transmitData != null) {
			success = BillingUtility.createReport(request, response, transmitData);
			log.info("GENERATEREPORT boolean success:"+success);
		}
		
		if(!success){
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "unable to generate report file");
			} catch (IOException e) {
				log.error("There was an error from GenerateReport servlet", e);
				return;
			}
		}		
	}
}