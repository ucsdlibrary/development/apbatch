package edu.ucsd.library.apbatch;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * @author lib-phnguyen
 * @author tchu
 */
public class EditIndexes extends HttpServlet {
	private static Logger log = Logger.getLogger(EditIndexes.class);

	/* client's request for server to perform an action */
	private static final String REQUEST_ACTION = "EDIT_INDEXES_REQUEST";

	/* actions client can request */
	private static final String REQUEST_ADD = "ADD";
	private static final String REQUEST_FETCH = "FETCH";
	private static final String REQUEST_EDIT = "EDIT";
	private static final String REQUEST_DELETE = "DELETE";
	private static final String REQUEST_DISPLAY = "DISPLAY";

	/* data labels expected from the client */
	private static final String HIDDEN_ID = "hidden_id";
	private static final String ENTITY = "entity";
	private static final String FUND = "fund";
	private static final String PROG = "prog";
	private static final String UNIT = "unit";
	private static final String ACCOUNT = "account";
	private static final String FUNCTION = "function";
	private static final String LOCATION = "location";
	private static final String PROJECT = "project";
	private static final String ACTIVITY = "activity";
	private static final String INTER_ENTITY = "inter_entity";
	private static final String FUTURE1 = "future1";
	private static final String FUTURE2 = "future2";
	private static final String DESCRIPTION = "description";
	private static final String ID = "id";
	private static final String TASK = "task";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response) {
		log.info("Determining what to do with request.");
		String requested_action = request.getParameter(REQUEST_ACTION);
		log.info("Client requested " + requested_action);

		if(requested_action.equalsIgnoreCase(REQUEST_ADD)) {
			handleAdd(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_FETCH)) {
			handleFetch(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_EDIT)) {
			handleEdit(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_DELETE)) {
			handleDelete(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_DISPLAY)) {
			handleDisplay(request, response);
		} else {
			log.info("Client's request cannot be handled.");
		}
	}

	public void handleAdd(HttpServletRequest request,HttpServletResponse response) {
		String entity = request.getParameter(ENTITY).trim();
		String fund = request.getParameter(FUND).trim();
		String unit = request.getParameter(UNIT).trim();
		String account = request.getParameter(ACCOUNT).trim();
		String function = request.getParameter(FUNCTION).trim();
		String prog = request.getParameter(PROG).trim();
		String location = request.getParameter(LOCATION).trim();
		String project = request.getParameter(PROJECT).trim();
		String activity = request.getParameter(ACTIVITY).trim();
		String inter_entity = request.getParameter(INTER_ENTITY).trim();
		String future1 = request.getParameter(FUTURE1).trim();
		String future2 = request.getParameter(FUTURE2).trim();
		String description = request.getParameter(DESCRIPTION).trim();
		String task = request.getParameter(TASK).trim();
		String id = null;
		
		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement(
					" INSERT INTO CHARTS_ACCOUNT (AC_ENTITY, AC_FUND, AC_FINANCIAL_UNIT, AC_ACT, AC_FUNCTION, " +
				    " AC_PROG, AC_LOC, AC_PROJECT, AC_ACTIVITY, AC_INTER_ENTITY, AC_FUTURE_1, AC_FUTURE_2, AC_DESCRIPTION, AC_TASK ) " +
					" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
					);

			pstmt.setString(1, entity);
			pstmt.setString(2, fund);
			pstmt.setString(3, unit);
			pstmt.setString(4, account);
			pstmt.setString(5, function);
			pstmt.setString(6, prog);
			pstmt.setString(7, location);
			pstmt.setString(8, project);
			pstmt.setString(9, activity);
			pstmt.setString(10, inter_entity);
			pstmt.setString(11, future1);
			pstmt.setString(12, future2);
			pstmt.setString(13, description);
			pstmt.setString(14, task);
			
			pstmt.executeUpdate();

			conn.commit();
			pstmt.close();
			
			pstmt = conn.prepareStatement("SELECT ID FROM CHARTS_ACCOUNT ORDER BY id DESC LIMIT 1");
			ResultSet rsTmp = pstmt.executeQuery();
			rsTmp.next();
			id = String.valueOf(rsTmp.getInt("ID"));
			conn.commit();
			pstmt.close();
			conn.setAutoCommit(true);
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter writer = new PrintWriter(response.getOutputStream());
			JSONObject results = new JSONObject();
			String htmlStr = "";
			htmlStr += "<tr id=\"editIndexes_checkbox_" + id + "\">";
			htmlStr += "<td>" + "<input type=\"checkbox\" name=\"editIndexes_checkboxes\"" +
					" value=\"" + id + "\"></td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + entity + "</td>";				
			htmlStr += "<td class=\"tc_border tc_align\">" + fund + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + unit + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + account + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + function + "</td>";				
			htmlStr += "<td class=\"tc_border tc_align\">" + prog + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + location + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + project + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + activity + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + inter_entity + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + future1 + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + future2 + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + description + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + task + "</td>";
			htmlStr += "</tr>";

			results.put("htmlStr", htmlStr);
			results.put("id", id);
			writer.write(results.toString());
			writer.close();

			log.info("Completing action for insertion of new fund info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for ADD");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(IOException ie) {
			log.info(ie.getMessage());
		}
	}

	public void handleFetch(HttpServletRequest request,HttpServletResponse response) {
		String id = request.getParameter(ID);

		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			pstmt = conn.prepareStatement(
					" SELECT * FROM CHARTS_ACCOUNT WHERE ID = ? ");

			pstmt.setInt(1, Integer.parseInt(id));

			ResultSet rsTmp = pstmt.executeQuery();
			rsTmp.next();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter writer = new PrintWriter(response.getOutputStream());
			JSONObject results = new JSONObject();
			results.put("id", id);
			results.put("entity", rsTmp.getString("AC_ENTITY"));			
			results.put("fund", rsTmp.getString("AC_FUND"));
			results.put("unit", rsTmp.getString("AC_FINANCIAL_UNIT"));
			results.put("account", rsTmp.getString("AC_ACT"));
			results.put("function", rsTmp.getString("AC_FUNCTION"));
			results.put("prog", rsTmp.getString("AC_PROG"));			
			results.put("location", rsTmp.getString("AC_LOC"));
			results.put("project", rsTmp.getString("AC_PROJECT"));
			results.put("activity", rsTmp.getString("AC_ACTIVITY"));
			results.put("inter_entity", rsTmp.getString("AC_INTER_ENTITY"));
			results.put("future1", rsTmp.getString("AC_FUTURE_1"));
			results.put("future2", rsTmp.getString("AC_FUTURE_2"));
			results.put("description", rsTmp.getString("AC_DESCRIPTION"));
			results.put("task", rsTmp.getString("AC_TASK"));
			writer.write(results.toString());
			writer.close();

			rsTmp.close();
			pstmt.close();
			conn.close();

			log.info("Fetched charts of account info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for FETCH");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(IOException ie) {
			log.info(ie.getMessage());
		}
	}
	
	public void handleEdit(HttpServletRequest request,HttpServletResponse response) {
		String hidden_id = request.getParameter(HIDDEN_ID).trim();
		String entity = request.getParameter(ENTITY).trim();
		String fund = request.getParameter(FUND).trim();
		String unit = request.getParameter(UNIT).trim();
		String account = request.getParameter(ACCOUNT).trim();
		String function = request.getParameter(FUNCTION).trim();
		String prog = request.getParameter(PROG).trim();
		String location = request.getParameter(LOCATION).trim();
		String project = request.getParameter(PROJECT).trim();
		String activity = request.getParameter(ACTIVITY).trim();
		String inter_entity = request.getParameter(INTER_ENTITY).trim();
		String future1 = request.getParameter(FUTURE1).trim();
		String future2 = request.getParameter(FUTURE2).trim();
		String description = request.getParameter(DESCRIPTION).trim();
		String task = request.getParameter(TASK).trim();

		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			conn.setAutoCommit(false);
	
			pstmt = conn.prepareStatement(
					" UPDATE CHARTS_ACCOUNT" +
					" SET AC_ENTITY = ?, AC_FUND = ?, AC_FINANCIAL_UNIT = ?, AC_ACT = ?, " +
					" AC_FUNCTION = ?, AC_PROG = ?, AC_LOC = ?, AC_PROJECT = ?, AC_ACTIVITY = ?, " +
					" AC_INTER_ENTITY = ?, AC_FUTURE_1 = ?, AC_FUTURE_2 = ?, AC_DESCRIPTION = ?, AC_TASK = ? " +
					" WHERE ID = ? "
					);

			pstmt.setString(1, entity);
			pstmt.setString(2, fund);
			pstmt.setString(3, unit);
			pstmt.setString(4, account);
			pstmt.setString(5, function);
			pstmt.setString(6, prog);
			pstmt.setString(7, location);
			pstmt.setString(8, project);
			pstmt.setString(9, activity);
			pstmt.setString(10, inter_entity);
			pstmt.setString(11, future1);
			pstmt.setString(12, future2);
			pstmt.setString(13, description);
			pstmt.setString(14, task);
			pstmt.setInt(15, Integer.parseInt(hidden_id));

			pstmt.executeUpdate();

			conn.commit();
			conn.setAutoCommit(true);

			pstmt.close();
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			try {
				PrintWriter writer = new PrintWriter(response.getOutputStream());
				JSONObject results = new JSONObject();
				String htmlStr = "";
				htmlStr += "<tr id=\"editIndexes_checkbox_" + hidden_id + "\">";
				htmlStr += "<td>" + "<input type=\"checkbox\" name=\"editIndexes_checkboxes\"" +
						" value=\"" + hidden_id + "\"></td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + entity + "</td>";				
				htmlStr += "<td class=\"tc_border tc_align\">" + fund + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + unit + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + account + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + function + "</td>";				
				htmlStr += "<td class=\"tc_border tc_align\">" + prog + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + location + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + project + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + activity + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + inter_entity + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + future1 + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + future2 + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + description + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + task + "</td>";
				htmlStr += "</tr>";
				results.put("id", hidden_id);
				results.put("hidden_id", hidden_id);
				results.put("htmlStr", htmlStr);
				writer.write(results.toString());
				writer.close();
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}

			log.info("Updated charts of account info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for EDIT");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(Exception ioe) {
			log.info(ioe.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for EDIT");
			} catch(Exception ie) {
				log.info(ie.getMessage());
			}
		}
	}

	public void handleDelete(HttpServletRequest request,HttpServletResponse response) {
		/* an array AC_FUND is expected in the form of a comma separated string
		 * EX: index : "1,2,3,4,...,n"
		 */
		String[] ids = ((String)request.getParameter(ID)).split(",");
		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement ps_delete = null;

		try {
			conn.setAutoCommit(false);

			ps_delete = conn.prepareStatement(
					" DELETE FROM CHARTS_ACCOUNT A WHERE A.ID = ? "
					);

			for(int i = 0; i < ids.length; ++i) {
				ps_delete.setInt(1, Integer.parseInt(ids[i]));
				ps_delete.executeUpdate();
			}

			conn.commit();
			conn.setAutoCommit(true);

			ps_delete.close();
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			try {
				PrintWriter writer = new PrintWriter(response.getOutputStream());
				JSONObject results = new JSONObject();
				results.put("status", "OKAY");
				writer.write(results.toString());
				writer.close();
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}

			log.info("Deleted from database row " + (String)request.getParameter(FUND));
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for DELETE");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		}
	}

	public void handleDisplay(HttpServletRequest request,HttpServletResponse response) {
		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			pstmt = conn.prepareStatement(
					" SELECT AC_ENTITY, AC_FUND, AC_FINANCIAL_UNIT, AC_ACT, AC_FUNCTION, AC_PROG, AC_LOC, " +
					" AC_PROJECT, AC_ACTIVITY, AC_INTER_ENTITY, AC_FUTURE_1, AC_FUTURE_2, AC_DESCRIPTION, AC_TASK, ID " +
					" FROM CHARTS_ACCOUNT ORDER BY AC_FUND "
					);

			ResultSet dataSet = pstmt.executeQuery();
			String htmlStr = "<table id=\"editIndexes_table\">" +
					"<thead>" +
						"<tr>" +
							"<th class=\"tc_align\"></th>" +
							"<th class=\"tc_align\">Entity</th>" +
							"<th class=\"tc_align\">Fund</th>" +
							"<th class=\"tc_align\">Financial Unit</th>" +
							"<th class=\"tc_align\">Account</th>" +
							"<th class=\"tc_align\">Function</th>" +
							"<th class=\"tc_align\">Program</th>" +
							"<th class=\"tc_align\">Location</th>" +
							"<th class=\"tc_align\">Project</th>" +
							"<th class=\"tc_align\">Activity</th>" +
							"<th class=\"tc_align\">Inter Entity</th>" +
							"<th class=\"tc_align\">Future1</th>" +
							"<th class=\"tc_align\">Future2</th>" +
							"<th class=\"tc_align\">Description</th>" +
							"<th class=\"tc_align\">Task</th>" +							
						"</tr>" +
					"</thead>" +
					"<tbody id=\"editIndexes_table_body\">";

			while(dataSet.next()) {
				htmlStr += "<tr id=\"editIndexes_checkbox_" + dataSet.getString(15) + "\">";

				htmlStr += "<td>" + "<input type=\"checkbox\" name=\"editIndexes_checkboxes\"" +
						" value=\"" + dataSet.getString(15) + "\"></td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(1) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(2) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(3) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(4) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(5) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(6) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(7) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(8) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(9) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(10) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(11) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(12) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(13) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(14) + "</td>";
				htmlStr += "</tr>";
			}
			htmlStr += "</tbody></table>";

			dataSet.close();
			pstmt.close();
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter writer = new PrintWriter(response.getOutputStream());
			JSONObject results = new JSONObject();
			results.put("htmlStr", htmlStr);
			writer.write(results.toString());
			writer.close();

			log.info("Completed creating table of charts of account info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete grabbing data for DISPLAY");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(IOException ie) {
			log.info(ie.getMessage());
		}
	}
}
