package edu.ucsd.library.apbatch;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * @author tchu
 */
public class EditPoetaf extends HttpServlet {
	private static Logger log = Logger.getLogger(EditPoetaf.class);

	/* client's request for server to perform an action */
	private static final String REQUEST_ACTION = "EDIT_POETAF_REQUEST";

	/* actions client can request */
	private static final String REQUEST_ADD = "ADD";
	private static final String REQUEST_FETCH = "FETCH";
	private static final String REQUEST_EDIT = "EDIT";
	private static final String REQUEST_DELETE = "DELETE";
	private static final String REQUEST_DISPLAY = "DISPLAY";

	/* data labels expected from the client */
	private static final String HIDDEN_ID = "hidden_id";
	private static final String PROJECT = "project";
	private static final String ORGANIZATION = "organization";
	private static final String TYPE = "type";
	private static final String TASK = "task";
	private static final String AWARD = "award";
	private static final String FUNDING = "funding";
	private static final String ID = "id";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response) {
		log.info("Determining what to do with request.");
		String requested_action = request.getParameter(REQUEST_ACTION);
		log.info("Client requested " + requested_action);

		if(requested_action.equalsIgnoreCase(REQUEST_ADD)) {
			handleAdd(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_FETCH)) {
			handleFetch(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_EDIT)) {
			handleEdit(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_DELETE)) {
			handleDelete(request, response);
		} else if(requested_action.equalsIgnoreCase(REQUEST_DISPLAY)) {
			handleDisplay(request, response);
		} else {
			log.info("Client's request cannot be handled.");
		}
	}

	public void handleAdd(HttpServletRequest request,HttpServletResponse response) {
		String project = request.getParameter(PROJECT).trim();
		String organization = request.getParameter(ORGANIZATION).trim();
		String type = request.getParameter(TYPE).trim();
		String task = request.getParameter(TASK).trim();
		String award = request.getParameter(AWARD).trim();
		String funding = request.getParameter(FUNDING).trim();
		String id = null;
		
		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement(
					" INSERT INTO POETAF (PROJECT_NUMBER, ORGANIZATION, TYPE, TASK_NUMBER, AWARD_NUMBER, FUNDING_SOURCE)" +
					" VALUES (?,?,?,?,?,?) "
					);

			pstmt.setString(1, project);
			pstmt.setString(2, organization);
			pstmt.setString(3, type);
			pstmt.setString(4, task);
			pstmt.setString(5, award);
			pstmt.setString(6, funding);
			
			pstmt.executeUpdate();

			conn.commit();
			pstmt.close();
			
			pstmt = conn.prepareStatement("SELECT ID FROM POETAF ORDER BY ID DESC LIMIT 1");
			ResultSet rsTmp = pstmt.executeQuery();
			rsTmp.next();
			id = String.valueOf(rsTmp.getInt("ID"));
			conn.commit();
			pstmt.close();
			conn.setAutoCommit(true);
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter writer = new PrintWriter(response.getOutputStream());
			JSONObject results = new JSONObject();
			String htmlStr = "";
			htmlStr += "<tr id=\"editPoetaf_checkbox_" + id + "\">";
			htmlStr += "<td>" + "<input type=\"checkbox\" name=\"editPoetaf_checkboxes\"" +
					" value=\"" + id + "\"></td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + project + "</td>";				
			htmlStr += "<td class=\"tc_border tc_align\">" + organization + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + type + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + task + "</td>";
			htmlStr += "<td class=\"tc_border tc_align\">" + award + "</td>";				
			htmlStr += "<td class=\"tc_border tc_align\">" + funding + "</td>";
			htmlStr += "</tr>";

			results.put("htmlStr", htmlStr);
			results.put("id", id);
			writer.write(results.toString());
			writer.close();

			log.info("Completing action for insertion of new project info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for ADD");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(IOException ie) {
			log.info(ie.getMessage());
		}
	}

	public void handleFetch(HttpServletRequest request,HttpServletResponse response) {
		String id = request.getParameter(ID);

		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			pstmt = conn.prepareStatement(
					" SELECT * FROM POETAF WHERE ID = ? ");

			pstmt.setInt(1, Integer.parseInt(id));

			ResultSet rsTmp = pstmt.executeQuery();
			rsTmp.next();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter writer = new PrintWriter(response.getOutputStream());
			JSONObject results = new JSONObject();
			results.put("id", id);
			results.put("project", rsTmp.getString("PROJECT_NUMBER"));			
			results.put("organization", rsTmp.getString("ORGANIZATION"));
			results.put("type", rsTmp.getString("TYPE"));
			results.put("task", rsTmp.getString("TASK_NUMBER"));
			results.put("award", rsTmp.getString("AWARD_NUMBER"));
			results.put("funding", rsTmp.getString("FUNDING_SOURCE"));
			writer.write(results.toString());
			writer.close();

			rsTmp.close();
			pstmt.close();
			conn.close();

			log.info("Fetched POETAF info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for FETCH");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(IOException ie) {
			log.info(ie.getMessage());
		}
	}
	
	public void handleEdit(HttpServletRequest request,HttpServletResponse response) {
		String hidden_id = request.getParameter(HIDDEN_ID).trim();
		String project = request.getParameter(PROJECT).trim();
		String organization = request.getParameter(ORGANIZATION).trim();
		String type = request.getParameter(TYPE).trim();
		String task = request.getParameter(TASK).trim();
		String award = request.getParameter(AWARD).trim();
		String funding = request.getParameter(FUNDING).trim();

		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			conn.setAutoCommit(false);
	
			pstmt = conn.prepareStatement(
					" UPDATE POETAF" +
					" SET PROJECT_NUMBER = ?, ORGANIZATION = ?, TYPE = ?, TASK_NUMBER = ?, " +
					" AWARD_NUMBER = ?, FUNDING_SOURCE = ? " +
					" WHERE ID = ? "
					);

			pstmt.setString(1, project);
			pstmt.setString(2, organization);
			pstmt.setString(3, type);
			pstmt.setString(4, task);
			pstmt.setString(5, award);
			pstmt.setString(6, funding);
			pstmt.setInt(7, Integer.parseInt(hidden_id));

			pstmt.executeUpdate();

			conn.commit();
			conn.setAutoCommit(true);

			pstmt.close();
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			try {
				PrintWriter writer = new PrintWriter(response.getOutputStream());
				JSONObject results = new JSONObject();
				String htmlStr = "";
				htmlStr += "<tr id=\"editPoetaf_checkbox_" + hidden_id + "\">";
				htmlStr += "<td>" + "<input type=\"checkbox\" name=\"editPoetaf_checkboxes\"" +
						" value=\"" + hidden_id + "\"></td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + project + "</td>";				
				htmlStr += "<td class=\"tc_border tc_align\">" + organization + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + type + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + task + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + award + "</td>";				
				htmlStr += "<td class=\"tc_border tc_align\">" + funding + "</td>";
				htmlStr += "</tr>";
				results.put("id", hidden_id);
				results.put("hidden_id", hidden_id);
				results.put("htmlStr", htmlStr);
				writer.write(results.toString());
				writer.close();
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}

			log.info("Updated POETAF info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for EDIT");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(Exception ioe) {
			log.info(ioe.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for EDIT");
			} catch(Exception ie) {
				log.info(ie.getMessage());
			}
		}
	}

	public void handleDelete(HttpServletRequest request,HttpServletResponse response) {
		/* an array AC_FUND is expected in the form of a comma separated string
		 * EX: index : "1,2,3,4,...,n"
		 */
		String[] ids = ((String)request.getParameter(ID)).split(",");
		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement ps_delete = null;

		try {
			conn.setAutoCommit(false);

			ps_delete = conn.prepareStatement(
					" DELETE FROM POETAF A WHERE A.ID = ? "
					);

			for(int i = 0; i < ids.length; ++i) {
				ps_delete.setInt(1, Integer.parseInt(ids[i]));
				ps_delete.executeUpdate();
			}

			conn.commit();
			conn.setAutoCommit(true);

			ps_delete.close();
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			try {
				PrintWriter writer = new PrintWriter(response.getOutputStream());
				JSONObject results = new JSONObject();
				results.put("status", "OKAY");
				writer.write(results.toString());
				writer.close();
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}

			log.info("Deleted from database row " + (String)request.getParameter(PROJECT));
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete transaction for DELETE");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		}
	}

	public void handleDisplay(HttpServletRequest request,HttpServletResponse response) {
		Connection conn = ApbatchConnection.getConnection();
		PreparedStatement pstmt = null;

		try {
			pstmt = conn.prepareStatement(
					" SELECT PROJECT_NUMBER, ORGANIZATION, TYPE, TASK_NUMBER, AWARD_NUMBER, FUNDING_SOURCE, ID " +
					" FROM POETAF ORDER BY PROJECT_NUMBER "
					);

			ResultSet dataSet = pstmt.executeQuery();
			String htmlStr = "<table id=\"editPoetaf_table\">" +
					"<thead>" +
						"<tr>" +
							"<th class=\"tc_align\"></th>" +
							"<th class=\"tc_align\">Project Number</th>" +
							"<th class=\"tc_align\">Organization</th>" +
							"<th class=\"tc_align\">Type</th>" +
							"<th class=\"tc_align\">Task Number</th>" +
							"<th class=\"tc_align\">Award Number</th>" +
							"<th class=\"tc_align\">Funding Source</th>" +
						"</tr>" +
					"</thead>" +
					"<tbody id=\"editPoetaf_table_body\">";

			while(dataSet.next()) {
				htmlStr += "<tr id=\"editPoetaf_checkbox_" + dataSet.getString(7) + "\">";

				htmlStr += "<td>" + "<input type=\"checkbox\" name=\"editPoetaf_checkboxes\"" +
						" value=\"" + dataSet.getString(7) + "\"></td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(1) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(2) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(3) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(4) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(5) + "</td>";
				htmlStr += "<td class=\"tc_border tc_align\">" + dataSet.getString(6) + "</td>";
				htmlStr += "</tr>";
			}
			htmlStr += "</tbody></table>";

			dataSet.close();
			pstmt.close();
			conn.close();

			response.setContentType("text/plain;charset=UTF-8");
			response.addHeader("Pragma", "no-cache");
			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter writer = new PrintWriter(response.getOutputStream());
			JSONObject results = new JSONObject();
			results.put("htmlStr", htmlStr);
			writer.write(results.toString());
			writer.close();

			log.info("Completed creating table of POETAF info.");
		} catch(SQLException se) {
			log.info(se.getMessage());
			try {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Database failed to complete grabbing data for DISPLAY");
			} catch(IOException ie) {
				log.info(ie.getMessage());
			}
		} catch(IOException ie) {
			log.info(ie.getMessage());
		}
	}
}