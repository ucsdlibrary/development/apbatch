package edu.ucsd.library.apbatch;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class SendEmail {
	private static Logger log = Logger.getLogger( SendEmail .class );
    private static String propertiesFilePath;

    public static void execute(String emailContent) throws Exception {
        Map<String,String> propertyMap = apBatchProperties();
        Properties props = defaultEmailProperties();

        final String username = propertyMap.get("email_username");
        final String password = propertyMap.get("email_password");
        final String to = propertyMap.get("email_to");
        final String from = propertyMap.get("email_from");
        props.put("mail.smtp.user", username);
        Session session = Session.getInstance(props, new UCSDAuthenticator(username, password));
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject("AP BATCH XLI4");
            message.setText(emailContent);

            Transport.send(message);

           log.info("execute: sent email message");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    // Load the properties from apbatch.properties to use in sending email in execute()
    private static Map<String,String> apBatchProperties() throws Exception {
        Map<String,String> propertyMap = new HashMap();
        try {
            InitialContext context = new InitialContext();
            final String propertiesFilePath = (String)context.lookup("java:comp/env/apbatchFilePath");
            FileReader reader = new FileReader(propertiesFilePath + "apbatch.properties");
            Properties p = new Properties();
            p.load(reader);

            Enumeration keys = p.propertyNames();
            while(keys.hasMoreElements()) {
                String key = (String)keys.nextElement();
                propertyMap.put(key,p.getProperty(key));
            }
            reader.close();
        } catch (Exception e) {
            log.info(e);
        }
        return propertyMap;
    }

    // Define default properties for sending email
    public static Properties defaultEmailProperties() {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.ucsd.edu");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth.mechanisms", "PLAIN");
        props.put("mail.debug", "true");
        return props;
    }
}

class UCSDAuthenticator extends Authenticator {
     String user;
     String pw;
     public UCSDAuthenticator (String username, String password)
     {
        super();
        this.user = username;
        this.pw = password;
     }
    public PasswordAuthentication getPasswordAuthentication()
    {
       return new PasswordAuthentication(user, pw);
    }
}
