#!/usr/bin/env sh

pg_port="5432"
while ! nc -z "$PGHOST" "$pg_port"
do
  echo "waiting for database..."
  sleep 1
done

echo "Injecting context secrets..."
sed -i -e "s/__PGUSER__/$PGUSER/g" \
    -i -e "s/__PGPASSWORD__/$PGPASSWORD/g" \
    -i -e "s/__PGHOST__/$PGHOST/g" \
    -i -e "s/__LDAP_USER__/$LDAP_USER/g" \
    -i -e "s/__LDAP_PASSWORD__/$LDAP_PASSWORD/g" "$CATALINA_HOME"/conf/Catalina/localhost/apbatch.xml
echo "Injecting QA API credentials..."
sed -i -e "s/__QA_CLIENT_SECRET__/$QA_CLIENT_SECRET/g" /apbatch/getApbatchTokenQA.sh

echo "Injecting Prod API credentials..."
sed -i -e "s/__PROD_CLIENT_SECRET__/$PROD_CLIENT_SECRET/g" /apbatch/getApbatchToken.sh

echo "Injecting Email credentials..."
sed -i -e "s/__EMAIL_USERNAME__/$EMAIL_USERNAME/g" /apbatch/apbatch.properties
sed -i -e "s/__EMAIL_PASSWORD__/$EMAIL_PASSWORD/g" /apbatch/apbatch.properties
sed -i -e "s/__EMAIL_FROM__/$EMAIL_FROM/g" /apbatch/apbatch.properties
sed -i -e "s/__EMAIL_TO__/$EMAIL_TO/g" /apbatch/apbatch.properties

exec "$@"

