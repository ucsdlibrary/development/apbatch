.PHONY: run menu

menu:
	@echo 'build: Build a AP Batch container image and push it to the k3d local registry'
	@echo 'clean: Delete the k3d/k3s cluster for AP Batch'
	@echo 'deploy: Deploy the AP Batch helm chart into the k3d/k3s cluster'
	@echo 'drop: Delete the namespace of the AP Batch development deployment'
	@echo 'redeploy: Build a new war file and deploy to tomcat running in cluster'
	@echo 'setup: Create a k3d/k3s cluster for AP Batch'
	@echo 'shell: Enter a shell for the running AP Batch application Pod'
	@echo 'test: Run the AP Batch test suite (currently a noop, no tests)'

clean:
	scripts/clean.sh

deploy: setup
	scripts/deploy.sh

drop: setup
	scripts/drop.sh

build: setup
	scripts/build.sh

redeploy:
	scripts/redeploy.sh

setup:
	scripts/setup.sh

shell:
	scripts/shell.sh

test:
	scripts/test.sh "$(TEST_ARGS)"
