# AP Batch

AP Batch is a Helm chart that leverages the [apbatch][apbatch] container
image to support easy deployment via Helm and Kubernetes.

## TL;DR;

```console
$ git clone https://gitlab.com/ucsdlibrary/development/apbatch.git
$ cd apbatch
$ helm dep update apbatch/
$ helm install my-release apbatch/
```

## Introduction

This chart bootstraps a [apbatch][apbatch] deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release apbatch
```

The command deploys AP Batch on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

The following tables lists the configurable parameters of the AP Batch chart and their default values, in addition to chart-specific options.

| Parameter | Description | Default | Environment Variable |
| --------- | ----------- | ------- | -------------------- |
| `image.repository` | apbatch image repository | `registry.gitlab.com/ucsdlibrary/development/apbatch` | N/A |
| `image.tag` | apbatch image tag to use | `stable` | N/A |
| `image.pullPolicy` | apbatch image pullPolicy | `Always` | N/A |
| `imagePullSecrets` | Array of pull secrets for the image | `[]` | N/A |
| `existingSecret.name` | Name of existing Secret in Deployment namespace to use instead of default Chart Secret.  | `apbatch` | `N/A` |
| `existingSecret.enabled` | Whether to use an existing Secret for Deployment rather than default Chart secret.  | `apbatch` | `N/A` |
| `oracleApi.qaSecret` | Secret key value for accessing Oracle QA API.  | `nil` | `QA_CLIENT_SECRET` |
| `oracleApi.prodSecret` | Secret key value for accessing Oracle Production API.  | `nil` | `PROD_CLIENT_SECRET` |
| `ldap.username` | LDAP username for authentication.  | `nil` | `LDAP_USER` |
| `ldap.password` | LDAP password for authentication.  | `nil` | `LDAP_PASSWORD` |
| `minio.accessKey` | Minio AccessKey for loading database backup.  | `nil` | `AWS_ACCESS_KEY_ID` |
| `minio.secretKey` | Minio SecretKey for loading database backup.  | `nil` | `AWS_SECRET_ACCESS_KEY` |
| `minio.bucket` | Minio bucket that holds database backup.  | `nil` | `BUCKET` |
| `minio.bucketKey` | Name of the database backup file in the bucket.  | `nil` | `BUCKET_KEY` |
| `minio.endpoint` | Minio endpoint URL.  | `nil` | `ENDPOINT_URL` |

#### PostgreSQL

By default, the chart will use a PostgreSQL helm chart for managing the local
database. For staging and production environments, it is critical to set the
`postgresql.postgresqlHostname` to the externally managed database URL, as well
as setting `postgresql.enabled` to `false`

See: https://github.com/bitnami/charts/blob/master/bitnami/postgresql/README.md

| Parameter | Description | Default | Environment Variable |
| --------- | ----------- | ------- | -------------------- |
| `postgresql.enabled` | Whether to use a Helm chart for PostgreSQL | `false` | N/A |
| `postgresql.postgresqlUsername` | Database user for application | `ap_user` | `PGUSER` |
| `postgresql.postgresqlPassword` | Database user password for application | `ap_pass` | `PGPASSWORD` |
| `postgresql.postgresqlDatabase` | Database name for application | `ap_user` | `PGDATABASE` |
| `postgresql.postgresqlHostname` | External hostname for PostgreSQL database. **Only** use when `postgresql.enabled` is set to `false | `nil` | `PGHOST` |
| `postgresql.postgresqlPostgresPassword` | Admin `postgres` user's password | `ap_admin` | `POSTGRES_ADMIN_PASSWORD` |
| `postgresql.persistence.size` | Database PVC size | `1Gi` | N/A |


[apbatch]:https://gitlab.com/ucsdlibrary/development/apbatch
